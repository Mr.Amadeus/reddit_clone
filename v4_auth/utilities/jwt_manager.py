from jose import jwt, JWTError
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from ..schemas import Token

SERVER_KEY = "2b2fd3a4106967471ea1781833fe0d5cdebc8cf06a7013bcac946555b2a8ec1a"
ALGORITHM = "HS256"


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth")

#encode token function
def generate_token(user_id: int):
    payload = {"user_id":user_id}
    encoded_jwt = jwt.encode(payload, SERVER_KEY, algorithm=ALGORITHM)
    return Token(access_token=encoded_jwt, token_type="bearer") 

#decode token function
def decode_token(provided_token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(provided_token, SERVER_KEY, algorithms=[ALGORITHM])
        decoded_id: str = payload.get("user_id")
    except JWTError:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return decoded_id
    
    
